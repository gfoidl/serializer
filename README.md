| CircleCi | AppVeyor |  
| -- | -- |  
| [![CircleCI](https://circleci.com/bb/gfoidl/serializer/tree/master.svg?style=svg)](https://circleci.com/bb/gfoidl/serializer/tree/master) | [![Build status](https://ci.appveyor.com/api/projects/status/q5b3k4m7016s3j67/branch/master?svg=true)](https://ci.appveyor.com/project/GntherFoidl/serializer/branch/master) |  

| Package | NuGet | MyGet |  
| -- | -- | -- |  
| gfoidl.Serializer | [![NuGet](https://img.shields.io/nuget/v/gfoidl.Serializer.svg?style=flat-square)](https://www.nuget.org/packages/gfoidl.Serializer/) | [![MyGet](https://img.shields.io/myget/gfoidl/v/gfoidl.Serializer.svg?style=flat-square)](https://www.myget.org/feed/gfoidl/package/nuget/gfoidl.Serializer) |  
| gfoidl.Serializer.Csv | [![NuGet](https://img.shields.io/nuget/v/gfoidl.Serializer.Csv.svg?style=flat-square)](https://www.nuget.org/packages/gfoidl.Serializer.Csv/) | [![MyGet](https://img.shields.io/myget/gfoidl/v/gfoidl.Serializer.Csv.svg?style=flat-square)](https://www.myget.org/feed/gfoidl/package/nuget/gfoidl.Serializer.Csv) |  
| gfoidl.Serializer.Excel | [![NuGet](https://img.shields.io/nuget/v/gfoidl.Serializer.Excel.svg?style=flat-square)](https://www.nuget.org/packages/gfoidl.Serializer.Excel/) | [![MyGet](https://img.shields.io/myget/gfoidl/v/gfoidl.Serializer.Excel.svg?style=flat-square)](https://www.myget.org/feed/gfoidl/package/nuget/gfoidl.Serializer.Excel) |  

# Serializer

## Intro

A generic component for serializing, and deserializing data to CSV, or Excel (xlsx).

The CSV-parser (deserializer) is ~1.5...2x faster than [TinyCsvParser](https://github.com/bytefish/TinyCsvParser) which claims itself as the fastest .net CSV parser around.

## Usage

```
public class Person
{
    public string Name { get; set; }
    public int Age     { get; set; }
}

private static ISerializer<Person> GetSerializer(bool excel = true)
{
    return excel ? new ExcelSerializer() : new CsvSerializer();
}

static void Main()
{
    Person[] persons =
    {
        new Person { Name = "Adam", Age = 31 },
        new Person { Name = "Eva" , Age = 29 }
    };
    
    ISerializer<Person> serializer = GetSerializer();

    using (MemoryStream stream = new MemoryStream())
    {
        serializer.Serialize(stream, persons);

        Debug.Assert(stream.Position > 0);

        stream.Position = 0;
        persons         = null;

        persons = serializer.Deserialize(stream).ToArray();
    }
}
```
