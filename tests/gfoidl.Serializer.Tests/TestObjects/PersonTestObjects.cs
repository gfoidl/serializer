﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace gfoidl.Serializer.Tests.TestObjects
{
    public interface IPerson
    {
        string Name   { get; set; }
        string Gender { get; set; }
        int Age       { get; set; }
    }
    //-------------------------------------------------------------------------
    public class Person : IPerson
    {
        public string Name   { get; set; }
        public string Gender { get; set; }
        public int Age       { get; set; }
    }
    //-------------------------------------------------------------------------
    public class Person1 : IPerson
    {
        public string Name   { get; set; }
        [DisplayName("Sex")]
        public string Gender { get; set; }
        public int Age       { get; set; }
    }
    //-------------------------------------------------------------------------
    public class Person11 : IPerson
    {
        public string Name   { get; set; }
        [Display(Name = "Sex")]
        public string Gender { get; set; }
        public int Age       { get; set; }
    }
    //-------------------------------------------------------------------------
    public class Person2 : IPerson
    {
        public string Name    { get; set; }
        public string Gender  { get; set; }
        public int Age        { get; set; }
        [SerializerIgnore]
        public string Address { get; set; }
    }
    //-------------------------------------------------------------------------
    public class Person3 : Person
    {
        public int YearsToHundred { get; set; }
    }
    //-------------------------------------------------------------------------
    public class Person4
    {
        public string Name   { get; set; }
        public string Gender { get; set; }
        public int? Age      { get; set; }
    }
    //-------------------------------------------------------------------------
    public class Person5
    {
        //[Display(Order = 0)]
        public string Name   { get; set; }
        [Display(Order = 2)]
        public string Gender { get; set; }
        [Display(Order = 1)]
        public int Age       { get; set; }
    }
    //---------------------------------------------------------------------
    public class Person6
    {
        [HideColumn, Display(Order = 0)]
        public Guid Id     { get; set; } = Guid.NewGuid();
        [Display(Order = 1)]
        public string Name { get; set; }
        [Display(Order = 2)]
        public int Age     { get; set; }
    }
    //-------------------------------------------------------------------------
    public class PersonEqualityComparer : EqualityComparer<IPerson>
    {
        public override bool Equals(IPerson x, IPerson y)
        {
            if (x == null && y == null) return true;
            if (x == null && y != null) return false;
            if (x != null && y == null) return false;

            return x.Name   == y.Name
                && x.Age    == y.Age
                && x.Gender == y.Gender;
        }
        //---------------------------------------------------------------------
        public override int GetHashCode(IPerson obj)
        {
            return obj.Name.GetHashCode() ^ obj.Age.GetHashCode() ^ obj.Gender.GetHashCode();
        }
    }
}
