﻿using System;

namespace gfoidl.Serializer.Tests.TestObjects
{
    public class PersonDateTimeTest
    {
        public string Name          { get; set; }
        public bool Male            { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Heigth           { get; set; }
        public double Weight        { get; set; }
    }
    //-------------------------------------------------------------------------
    public class PersonDateTimeTest1
    {
        public string Name          { get; set; }
        public bool Male            { get; set; }
        [DateTimeFormat(true)]
        public DateTime DateOfBirth { get; set; }
        public int Heigth           { get; set; }
        public double Weight        { get; set; }
    }
}
