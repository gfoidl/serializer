﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using gfoidl.Serializer.Tests.TestObjects;
using NUnit.Framework;

namespace gfoidl.Serializer.Excel.Tests.ExcelSerializerTests
{
    [TestFixture]
    public class Deserialize
    {
        [Test]
        public void Excel_given___OK()
        {
            ExcelSerializer<Person> sut = new ExcelSerializer<Person>();

            using (FileStream stream = File.OpenRead("data/input_1.xlsx"))
            {
                List<Person> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                Person p1 = new Person { Name = "Günther" , Gender = "Male"  , Age = 32 };
                Person p2 = new Person { Name = "Isabella", Gender = "Female", Age = 20 };

                IEqualityComparer<Person> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Excel_with_different_column_order_given___OK()
        {
            ExcelSerializer<Person> sut = new ExcelSerializer<Person>();

            using (FileStream stream = File.OpenRead("data/input_2.xlsx"))
            {
                List<Person> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                Person p1 = new Person { Name = "Günther" , Gender = "Male"  , Age = 32 };
                Person p2 = new Person { Name = "Isabella", Gender = "Female", Age = 20 };

                IEqualityComparer<Person> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Diplayname___OK()
        {
            ExcelSerializer<Person1> sut = new ExcelSerializer<Person1>();

            using (FileStream stream = File.OpenRead("data/input_3.xlsx"))
            {
                List<Person1> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                Person1 p1 = new Person1 { Name = "Günther" , Gender = "Male"  , Age = 32 };
                Person1 p2 = new Person1 { Name = "Isabella", Gender = "Female", Age = 20 };

                IEqualityComparer<Person1> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Diplayname_from_DisplayAttribute___OK()
        {
            ExcelSerializer<Person11> sut = new ExcelSerializer<Person11>();

            using (FileStream stream = File.OpenRead("data/input_3.xlsx"))
            {
                List<Person11> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                Person11 p1 = new Person11 { Name = "Günther" , Gender = "Male"  , Age = 32 };
                Person11 p2 = new Person11 { Name = "Isabella", Gender = "Female", Age = 20 };

                IEqualityComparer<Person11> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Lookup_and_DisplayName___Lookup_is_used()
        {
            Func<string, string> lookupPropertyName = display =>
            {
                if (display == "Male/Female") return "Gender";

                return display;
            };

            ExcelSerializer<Person1> sut = new ExcelSerializer<Person1>(lookupPropertyNameFromDisplayName: lookupPropertyName);

            using (FileStream stream = File.OpenRead("data/input_4.xlsx"))
            {
                List<Person1> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                Person1 p1 = new Person1 { Name = "Günther" , Gender = "Male"  , Age = 32 };
                Person1 p2 = new Person1 { Name = "Isabella", Gender = "Female", Age = 20 };

                IEqualityComparer<Person1> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Property_for_column_does_not_exist___throws_Exception()
        {
            ExcelSerializer<Person> sut = new ExcelSerializer<Person>();

            Assert.Throws<PropertyForColumnNotFoundException<Person>>(() =>
            {
                using (FileStream stream = File.OpenRead("data/input_3.xlsx"))
                {
                    List<Person> actual = sut.Deserialize(stream).ToList();

                    Assert.AreEqual(2, actual.Count);

                    Person p1 = new Person { Name = "Günther" , Gender = "Male"  , Age = 32 };
                    Person p2 = new Person { Name = "Isabella", Gender = "Female", Age = 20 };

                    IEqualityComparer<Person> personComparer = new PersonEqualityComparer();

                    Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                    Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
                }
            });
        }
        //---------------------------------------------------------------------
        [Test]
        public void SerializerIgnore___property_ignored()
        {
            ExcelSerializer<Person2> sut = new ExcelSerializer<Person2>();

            using (FileStream stream = File.OpenRead("data/input_1.xlsx"))
            {
                List<Person2> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                Person2 p1 = new Person2 { Name = "Günther" , Gender = "Male"  , Age = 32 };
                Person2 p2 = new Person2 { Name = "Isabella", Gender = "Female", Age = 20 };

                IEqualityComparer<Person2> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void CellType_Formula___Cell_Value_read()
        {
            ExcelSerializer<Person3> sut = new ExcelSerializer<Person3>();

            using (FileStream stream = File.OpenRead("data/input_5.xlsx"))
            {
                List<Person3> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                Person3 p1 = new Person3 { Name = "Günther" , Gender = "Male"  , Age = 32 };
                Person3 p2 = new Person3 { Name = "Isabella", Gender = "Female", Age = 20 };

                IEqualityComparer<Person3> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));

                Assert.AreEqual(100 - 32, actual[0].YearsToHundred);
                Assert.AreEqual(100 - 20, actual[1].YearsToHundred);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Cell_is_empty___null_is_used()
        {
            ExcelSerializer<Person4> sut = new ExcelSerializer<Person4>();

            using (FileStream stream = File.OpenRead("data/input_6.xlsx"))
            {
                List<Person4> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                Assert.IsNull(actual[0].Age);
                Assert.AreEqual(20, actual[1].Age);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Type_with_bool_property_Issue10___OK()
        {
            ExcelSerializer<PersonDateTimeTest> sut = new ExcelSerializer<PersonDateTimeTest>();

            using (FileStream stream = File.OpenRead("data/input_7.xlsx"))
            {
                List<PersonDateTimeTest> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(1, actual.Count);

                Assert.AreEqual("Günther", actual[0].Name);
                Assert.IsTrue(actual[0].Male);
                Assert.AreEqual(new DateTime(1982, 7, 22), actual[0].DateOfBirth);
            }
        }
    }
}
