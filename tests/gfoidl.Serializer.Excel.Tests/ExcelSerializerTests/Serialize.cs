﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using gfoidl.Serializer.Tests.TestObjects;
using NPOI.SS.UserModel;
using NUnit.Framework;

namespace gfoidl.Serializer.Excel.Tests.ExcelSerializerTests
{
    [TestFixture]
    public class Serialize
    {
        [Test]
        public void Model_given___OK()
        {
            ExcelSerializer<Person> sut = new ExcelSerializer<Person>();

            Person p1 = new Person { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person p2 = new Person { Name = "Isabella", Gender = "Female", Age = 20 };

            Person[] persons = { p1, p2 };

            using (FileStream stream = File.Create("data/output_1.xlsx"))
                sut.Serialize(stream, persons);

            using (FileStream stream = File.OpenRead("data/output_1.xlsx"))
            {
                List<Person> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                IEqualityComparer<Person> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Model_given___first_row_is_bold()
        {
            ExcelSerializer<Person> sut = new ExcelSerializer<Person>();

            Person p1 = new Person { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person p2 = new Person { Name = "Isabella", Gender = "Female", Age = 20 };

            Person[] persons = { p1, p2 };

            using (FileStream stream = File.Create("data/output_1.xlsx"))
                sut.Serialize(stream, persons);

            using (FileStream stream = File.OpenRead("data/output_1.xlsx"))
            {
                IWorkbook workBook = WorkbookFactory.Create(stream);
                ISheet sheet       = workBook.GetSheetAt(0);
                IRow headerRow     = sheet.GetRow(0);
                IFont font         = headerRow.RowStyle.GetFont(workBook);

                Assert.IsTrue(font.IsBold);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Displayname_write___OK()
        {
            ExcelSerializer<Person1> sut = new ExcelSerializer<Person1>();

            Person1 p1 = new Person1 { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person1 p2 = new Person1 { Name = "Isabella", Gender = "Female", Age = 20 };

            Person1[] persons = { p1, p2 };

            using (FileStream stream = File.Create("data/output_2.xlsx"))
                sut.Serialize(stream, persons);

            using (FileStream stream = File.OpenRead("data/output_2.xlsx"))
            {
                IWorkbook workBook = WorkbookFactory.Create(stream);
                ISheet sheet       = workBook.GetSheetAt(0);
                IRow row           = sheet.GetRow(0);

                string[] actual   = row.Cells.Select(c => c.StringCellValue).ToArray();
                string[] expected = { "Name", "Sex", "Age" };

                CollectionAssert.AreEqual(expected, actual);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Displayname_from_DisplayAttribute_write___OK()
        {
            ExcelSerializer<Person11> sut = new ExcelSerializer<Person11>();

            Person11 p1 = new Person11 { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person11 p2 = new Person11 { Name = "Isabella", Gender = "Female", Age = 20 };

            Person11[] persons = { p1, p2 };

            using (FileStream stream = File.Create("data/output_2.1.xlsx"))
                sut.Serialize(stream, persons);

            using (FileStream stream = File.OpenRead("data/output_2.1.xlsx"))
            {
                IWorkbook workBook = WorkbookFactory.Create(stream);
                ISheet sheet       = workBook.GetSheetAt(0);
                IRow row           = sheet.GetRow(0);

                string[] actual   = row.Cells.Select(c => c.StringCellValue).ToArray();
                string[] expected = { "Name", "Sex", "Age" };

                CollectionAssert.AreEqual(expected, actual);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Lookup_and_DisplayName___Lookup_is_used()
        {
            Func<string, string> lookupDisplayName = prop =>
            {
                if (prop == "Gender") return "Male/Female";

                return prop;
            };

            Func<string, string> lookupPropertyName = display =>
            {
                if (display == "Male/Female") return "Gender";

                return display;
            };

            ExcelSerializer<Person1> sut = new ExcelSerializer<Person1>(
                lookupDisplayNameFromPropertyName: lookupDisplayName,
                lookupPropertyNameFromDisplayName: lookupPropertyName);

            Person1 p1 = new Person1 { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person1 p2 = new Person1 { Name = "Isabella", Gender = "Female", Age = 20 };

            Person1[] persons = { p1, p2 };

            using (FileStream stream = File.Create("data/output_3.xlsx"))
                sut.Serialize(stream, persons);

            using (FileStream stream = File.OpenRead("data/output_3.xlsx"))
            {
                List<Person1> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                IEqualityComparer<Person1> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void SerializerIgnore___property_ignored()
        {
            ExcelSerializer<Person2> sut = new ExcelSerializer<Person2>();

            Person2 p1 = new Person2 { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person2 p2 = new Person2 { Name = "Isabella", Gender = "Female", Age = 20 };

            Person2[] persons = { p1, p2 };

            using (FileStream stream = File.Create("data/output_4.xlsx"))
                sut.Serialize(stream, persons);

            using (FileStream stream = File.OpenRead("data/output_4.xlsx"))
            {
                List<Person2> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                IEqualityComparer<Person2> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void DateTime_Issue4___Cell_shows_Date()
        {
            ExcelSerializer<PersonDateTimeTest> sut = new ExcelSerializer<PersonDateTimeTest>();

            PersonDateTimeTest p1 = new PersonDateTimeTest { Name = "Günther", DateOfBirth = new DateTime(1982, 7, 22, 23, 25, 30), Male = true, Weight = 105.2, Heigth = 184 };

            PersonDateTimeTest[] persons = { p1 };

            using (FileStream stream = File.Create("data/output_5.xlsx"))
                sut.Serialize(stream, persons);

            using (FileStream stream = File.OpenRead("data/output_5.xlsx"))
            {
                IWorkbook workBook = WorkbookFactory.Create(stream);
                ISheet sheet       = workBook.GetSheetAt(0);
                IRow row           = sheet.GetRow(1);

                Assert.AreEqual(@"DD/MM/YYYY\ HH:MM:SS", row.Cells[2].CellStyle.GetDataFormatString());
                var actual = row.GetCell(2).DateCellValue;
                Assert.AreEqual(new DateTime(1982, 7, 22, 23, 25, 30), actual);
            }

            using (FileStream stream = File.OpenRead("data/output_5.xlsx"))
            {
                List<PersonDateTimeTest> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(1, actual.Count);
                Assert.AreEqual("Günther", actual[0].Name);
                Assert.AreEqual(new DateTime(1982, 7, 22, 23, 25, 30), actual[0].DateOfBirth);
                Assert.IsTrue(actual[0].Male);
                Assert.AreEqual(105.2, actual[0].Weight, 1e-3);
                Assert.AreEqual(184, actual[0].Heigth);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void DateTime_only_DatePart___OK()
        {
            ExcelSerializer<PersonDateTimeTest1> sut = new ExcelSerializer<PersonDateTimeTest1>();

            PersonDateTimeTest1 p1 = new PersonDateTimeTest1 { Name = "Günther", DateOfBirth = new DateTime(1982, 7, 22, 23, 25, 30), Male = true, Weight = 105.2, Heigth = 184 };

            PersonDateTimeTest1[] persons = { p1 };

            using (FileStream stream = File.Create("data/output_6.xlsx"))
                sut.Serialize(stream, persons);

            using (FileStream stream = File.OpenRead("data/output_6.xlsx"))
            {
                IWorkbook workBook = WorkbookFactory.Create(stream);
                ISheet sheet       = workBook.GetSheetAt(0);
                IRow row           = sheet.GetRow(1);

                Assert.AreEqual(@"DD/MM/YYYY", row.GetCell(2).CellStyle.GetDataFormatString());

                var actual = row.GetCell(2).DateCellValue;
                Assert.AreEqual(new DateTime(1982, 7, 22), actual);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void DateTime_with_Zero_Time___written_as_DateTime_with_Time()
        {
            ExcelSerializer<PersonDateTimeTest> sut = new ExcelSerializer<PersonDateTimeTest>();

            PersonDateTimeTest p1 = new PersonDateTimeTest { Name = "Günther", DateOfBirth = new DateTime(1982, 7, 22, 0, 0, 0), Male = true, Weight = 105.2, Heigth = 184 };

            PersonDateTimeTest[] persons = { p1 };

            using (FileStream stream = File.Create("data/output_7.xlsx"))
                sut.Serialize(stream, persons);

            using (FileStream stream = File.OpenRead("data/output_7.xlsx"))
            {
                IWorkbook workBook = WorkbookFactory.Create(stream);
                ISheet sheet       = workBook.GetSheetAt(0);
                IRow row           = sheet.GetRow(1);

                Assert.AreEqual(@"DD/MM/YYYY\ HH:MM:SS", row.Cells[2].CellStyle.GetDataFormatString());
                var actual = row.GetCell(2).DateCellValue;
                Assert.AreEqual(new DateTime(1982, 7, 22), actual);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Model_with_null_value_given___OK()
        {
            ExcelSerializer<Person4> sut = new ExcelSerializer<Person4>();

            Person4 p1 = new Person4 { Name = "Günther" , Gender = "Male"  , Age = null };
            Person4 p2 = new Person4 { Name = "Isabella", Gender = "Female", Age = 20 };

            Person4[] persons = { p1, p2 };

            using (FileStream stream = File.Create("data/output_8.xlsx"))
            {
                sut.Serialize(stream, persons);
            }

            using (FileStream stream = File.OpenRead("data/output_8.xlsx"))
            {
                List<Person4> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                Assert.IsNull(actual[0].Age);
                Assert.AreEqual(20, actual[1].Age);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Display_Order___order_is_in_output()
        {
            ExcelSerializer<Person5> sut = new ExcelSerializer<Person5>();

            Person5 p1 = new Person5 { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person5 p2 = new Person5 { Name = "Isabella", Gender = "Female", Age = 20 };

            Person5[] persons = { p1, p2 };

            using (FileStream stream = File.Create("data/output_9.xlsx"))
            {
                sut.Serialize(stream, persons);
            }

            using (FileStream stream = File.OpenRead("data/output_9.xlsx"))
            {
                IWorkbook workBook = WorkbookFactory.Create(stream);
                ISheet sheet       = workBook.GetSheetAt(0);
                IRow row           = sheet.GetRow(0);

                string[] actual   = row.Cells.Select(c => c.StringCellValue).ToArray();
                string[] expected = { "Name", "Age", "Gender" };

                CollectionAssert.AreEqual(expected, actual);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Serializer_for_two_different_types___OK()
        {
            ExcelSerializer<Person> sut1             = new ExcelSerializer<Person>();
            ExcelSerializer<PersonDateTimeTest> sut2 = new ExcelSerializer<PersonDateTimeTest>();

            Person p1 = new Person { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person p2 = new Person { Name = "Isabella", Gender = "Female", Age = 20 };

            PersonDateTimeTest pdt1 = new PersonDateTimeTest { Name = "Günther", DateOfBirth = new DateTime(1982, 7, 22, 0, 0, 0), Male = true, Weight = 105.2, Heigth = 184 };

            Person[] persons               = { p1, p2 };
            PersonDateTimeTest[] personsDt = { pdt1 };

            using (FileStream stream = File.Create("data/output_11.xlsx"))
                sut1.Serialize(stream, persons);

            using (FileStream stream = File.OpenRead("data/output_11.xlsx"))
            {
                List<Person> actual1 = sut1.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual1.Count);

                IEqualityComparer<Person> personComparer = new PersonEqualityComparer();

                Assert.That(actual1[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual1[1], Is.EqualTo(p2).Using(personComparer));
            }

            using (FileStream stream = File.Create("data/output_12.xlsx"))
                sut2.Serialize(stream, personsDt);

            using (FileStream stream = File.OpenRead("data/output_12.xlsx"))
            {
                List<PersonDateTimeTest> actual2 = sut2.Deserialize(stream).ToList();

                Assert.AreEqual(1, actual2.Count);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Sheetname_given___Sheetname_is_set()
        {
            ExcelSerializer<Person> sut = new ExcelSerializer<Person>();

            Person p1 = new Person { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person p2 = new Person { Name = "Isabella", Gender = "Female", Age = 20 };

            Person[] persons = { p1, p2 };

            using (FileStream stream = File.Create("data/output_10.xlsx"))
                sut.Serialize(stream, persons, "Foo-Bar");

            using (FileStream stream = File.OpenRead("data/output_10.xlsx"))
            {
                IWorkbook workBook = WorkbookFactory.Create(stream);
                ISheet sheet       = workBook.GetSheetAt(0);

                Assert.AreEqual("Foo-Bar", sheet.SheetName);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void HideColumn_set_on_Id_property___Column_is_hidden()
        {
            ExcelSerializer<Person6> sut = new ExcelSerializer<Person6>();

            Person6 p1 = new Person6 { Name = "Günther" , Age = 32 };
            Person6 p2 = new Person6 { Name = "Isabella", Age = 20 };

            Person6[] persons = { p1, p2 };

            using (FileStream stream = File.Create("data/output_13.xlsx"))
                sut.Serialize(stream, persons);

            using (FileStream stream = File.OpenRead("data/output_13.xlsx"))
            {
                IWorkbook workBook = WorkbookFactory.Create(stream);
                ISheet sheet       = workBook.GetSheetAt(0);

                Assert.IsTrue(sheet.IsColumnHidden(0));
                Assert.IsFalse(sheet.IsColumnHidden(1));
                Assert.IsFalse(sheet.IsColumnHidden(2));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void HideColumn_set_on_Id_property___Column_can_be_Deserialized()
        {
            ExcelSerializer<Person6> sut = new ExcelSerializer<Person6>();

            Person6 p1 = new Person6 { Name = "Günther" , Age = 32, Id = Guid.NewGuid() };
            Person6 p2 = new Person6 { Name = "Isabella", Age = 20, Id = Guid.NewGuid() };

            Person6[] persons = { p1, p2 };

            using (FileStream stream = File.Create("data/output_13.1.xlsx"))
                sut.Serialize(stream, persons);

            using (FileStream stream = File.OpenRead("data/output_13.1.xlsx"))
            {
                List<Person6> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(p1.Id, actual[0].Id);
                Assert.AreEqual(p2.Id, actual[1].Id);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Serialization_to_MemoryStream___Stream_is_not_disposed_Issue13()
        {
            ExcelSerializer<Person> sut = new ExcelSerializer<Person>();

            Person p1 = new Person { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person p2 = new Person { Name = "Isabella", Gender = "Female", Age = 20 };

            Person[] persons = { p1, p2 };

            using (MemoryStream stream = new MemoryStream())
            {
                sut.Serialize(stream, persons);

                Assert.That(stream.Length, Is.GreaterThan(0));
            }
        }
    }
}
