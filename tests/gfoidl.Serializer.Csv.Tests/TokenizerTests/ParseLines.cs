﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace gfoidl.Serializer.Csv.Tests.TokenizerTests
{
    [TestFixture(1)]
    [TestFixture(2)]
    [TestFixture(1024)]
    [TestFixture(4096)]
    public class ParseLines
    {
        private readonly int _bufferSize;
        //---------------------------------------------------------------------
        public ParseLines(int bufferSize) => _bufferSize = bufferSize;
        //---------------------------------------------------------------------
        [Test]
        public void Empty_data___empty_collection()
        {
            TextReader tr        = new StringReader("");
            const char separator = ';';

            var sut = new Tokenizer(tr, 2, separator);

            List<Line> actual = sut.ParseLines().ToList();

            Assert.AreEqual(0, actual.Count);
        }
        //---------------------------------------------------------------------
        [Test]
        [TestCase("ab;de\nxy;yz")]
        [TestCase("ab;de\nxy;yz\n")]
        [TestCase("ab;de\r\nxy;yz")]
        [TestCase("ab;de\r\nxy;yz\r\n")]
        [TestCase("ab;de\rxy;yz")]
        [TestCase("ab;de\rxy;yz\r")]
        public void Data_given___OK(string text)
        {
            TextReader tr        = new StringReader(text);
            const char separator = ';';
            string[][] expected  =
            {
                new[] { "ab", "de" },
                new[] { "xy", "yz" }
            };

            var sut = new Tokenizer(tr, 2, separator, _bufferSize);

            List<Line> actual = sut.ParseLines().ToList();

            Assert.AreEqual(2, actual.Count);

            string[] cols0 = actual[0].GetColumns().Select(c => c.Value).ToArray();
            string[] cols1 = actual[1].GetColumns().Select(c => c.Value).ToArray();

            CollectionAssert.AreEqual(expected[0], cols0);
            CollectionAssert.AreEqual(expected[1], cols1);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Single_row_no_trailing_empty_line___OK()
        {
            TextReader tr        = new StringReader("ab;de");
            const char separator = ';';
            string[] expected    = { "ab", "de" };

            var sut = new Tokenizer(tr, 2, separator, _bufferSize);

            List<Line> actual = sut.ParseLines().ToList();

            Assert.AreEqual(1, actual.Count);

            string[] cols = actual[0].GetColumns().Select(c => c.Value).ToArray();

            CollectionAssert.AreEqual(expected, cols);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Single_row_trailing_empty_line___OK([Values("\n", "\r", "\r\n")] string eol)
        {
            TextReader tr        = new StringReader($"ab;de{eol}");
            const char separator = ';';
            string[] expected    = { "ab", "de" };

            var sut = new Tokenizer(tr, 2, separator, _bufferSize);

            List<Line> actual = sut.ParseLines().ToList();

            Assert.AreEqual(1, actual.Count);

            string[] cols = actual[0].GetColumns().Select(c => c.Value).ToArray();

            CollectionAssert.AreEqual(expected, cols);
        }
    }
}
