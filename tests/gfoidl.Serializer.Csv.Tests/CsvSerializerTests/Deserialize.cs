﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using gfoidl.Serializer.Tests.TestObjects;
using NUnit.Framework;

namespace gfoidl.Serializer.Csv.Tests.CsvSerializerTest
{
    [TestFixture, Parallelizable(ParallelScope.Fixtures)]
    public class Deserialize
    {
        [Test]
        public void Csv_given___OK()
        {
            CsvSerializer<Person> sut = new CsvSerializer<Person>();

            using (FileStream stream = File.OpenRead("data/input_1.csv"))
            {
                List<Person> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                Person p1 = new Person { Name = "Günther" , Gender = "Male"  , Age = 32 };
                Person p2 = new Person { Name = "Isabella", Gender = "Female", Age = 20 };

                IEqualityComparer<Person> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Different_column_order___OK()
        {
            CsvSerializer<Person> sut = new CsvSerializer<Person>();

            using (FileStream stream = File.OpenRead("data/input_2.csv"))
            {
                List<Person> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                Person p1 = new Person { Name = "Günther" , Gender = "Male"  , Age = 32 };
                Person p2 = new Person { Name = "Isabella", Gender = "Female", Age = 20 };

                IEqualityComparer<Person> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Diplayname___OK()
        {
            CsvSerializer<Person1> sut = new CsvSerializer<Person1>();

            using (FileStream stream = File.OpenRead("data/input_3.csv"))
            {
                List<Person1> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                Person1 p1 = new Person1 { Name = "Günther" , Gender = "Male"  , Age = 32 };
                Person1 p2 = new Person1 { Name = "Isabella", Gender = "Female", Age = 20 };

                IEqualityComparer<Person1> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Diplayname_from_DisplayAttribute___OK()
        {
            CsvSerializer<Person11> sut = new CsvSerializer<Person11>();

            using (FileStream stream = File.OpenRead("data/input_3.csv"))
            {
                List<Person11> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                Person11 p1 = new Person11 { Name = "Günther" , Gender = "Male"  , Age = 32 };
                Person11 p2 = new Person11 { Name = "Isabella", Gender = "Female", Age = 20 };

                IEqualityComparer<Person11> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Lookup_and_DisplayName___Lookup_is_used()
        {
            Func<string, string> lookupPropertyName = display =>
            {
                if (display == "Male/Female") return "Gender";

                return display;
            };

            CsvSerializer<Person1> sut = new CsvSerializer<Person1>(lookupPropertyNameFromDisplayName: lookupPropertyName);         

            using (FileStream stream = File.OpenRead("data/input_4.csv"))
            {
                List<Person1> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                Person1 p1 = new Person1 { Name = "Günther" , Gender = "Male"  , Age = 32 };
                Person1 p2 = new Person1 { Name = "Isabella", Gender = "Female", Age = 20 };

                IEqualityComparer<Person1> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Property_for_column_does_not_exist___throws_Exception()
        {
            CsvSerializer<Person> sut = new CsvSerializer<Person>();

            Assert.Throws<PropertyForColumnNotFoundException<Person>>(() =>
            {
                using (FileStream stream = File.OpenRead(@"data/input_3.csv"))
                {
                    List<Person> actual = sut.Deserialize(stream).ToList();

                    Assert.AreEqual(2, actual.Count);

                    Person p1 = new Person { Name = "Günther" , Gender = "Male"  , Age = 32 };
                    Person p2 = new Person { Name = "Isabella", Gender = "Female", Age = 20 };

                    IEqualityComparer<Person> personComparer = new PersonEqualityComparer();

                    Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                    Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
                }
            });
        }
        //---------------------------------------------------------------------
        [Test]
        public void SerializerIgnore___property_ignored()
        {
            CsvSerializer<Person2> sut = new CsvSerializer<Person2>();

            using (FileStream stream = File.OpenRead("data/input_1.csv"))
            {
                List<Person2> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                Person2 p1 = new Person2 { Name = "Günther" , Gender = "Male"  , Age = 32 };
                Person2 p2 = new Person2 { Name = "Isabella", Gender = "Female", Age = 20 };

                IEqualityComparer<Person2> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Cell_is_empty___null_is_used()
        {
            CsvSerializer<Person4> sut = new CsvSerializer<Person4>();

            using (FileStream stream = File.OpenRead("data/input_6.csv"))
            {
                List<Person4> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                Assert.IsNull(actual[0].Age);
                Assert.AreEqual(20, actual[1].Age);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Type_with_bool_property_Issue10___OK()
        {
            CultureInfo curCulture                = Thread.CurrentThread.CurrentCulture;
            CultureInfo curUiCulture              = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentCulture   = CultureInfo.GetCultureInfo("de");
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("de");

            try
            {
                CsvSerializer<PersonDateTimeTest> sut = new CsvSerializer<PersonDateTimeTest>();

                using (FileStream stream = File.OpenRead("data/input_7.csv"))
                {
                    List<PersonDateTimeTest> actual = sut.Deserialize(stream).ToList();

                    Assert.AreEqual(1, actual.Count);

                    Assert.AreEqual("Günther", actual[0].Name);
                    Assert.IsTrue(actual[0].Male);
                    Assert.AreEqual(new DateTime(1982, 7, 22), actual[0].DateOfBirth);
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture   = curCulture;
                Thread.CurrentThread.CurrentUICulture = curUiCulture;
            }
        }
    }
}
