﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using gfoidl.Serializer.Tests.TestObjects;
using NUnit.Framework;

namespace gfoidl.Serializer.Csv.Tests.CsvSerializerTests
{
    [TestFixture, Parallelizable(ParallelScope.Fixtures)]
    public class Serialize
    {
        [Test]
        public void Model_given___OK()
        {
            CsvSerializer<Person> sut = new CsvSerializer<Person>();

            Person p1 = new Person { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person p2 = new Person { Name = "Isabella", Gender = "Female", Age = 20 };

            Person[] persons = { p1, p2 };

            using (FileStream stream = File.Create("data/output_1.csv"))
            {
                sut.Serialize(stream, persons);
            }

            using (FileStream stream = File.OpenRead("data/output_1.csv"))
            {
                List<Person> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                IEqualityComparer<Person> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Displayname_write___OK()
        {
            CsvSerializer<Person1> sut = new CsvSerializer<Person1>();

            Person1 p1 = new Person1 { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person1 p2 = new Person1 { Name = "Isabella", Gender = "Female", Age = 20 };

            Person1[] persons = { p1, p2 };

            using (FileStream stream = File.Create("data/output_2.csv"))
                sut.Serialize(stream, persons);

            using (StreamReader sr = File.OpenText("data/output_2.csv"))
            {
                string[] actual   = sr.ReadLine().Split(';');
                string[] expected = { "Name", "Sex", "Age" };

                CollectionAssert.AreEqual(expected, actual);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Displayname_from_DisplayAttribute_write___OK()
        {
            CsvSerializer<Person11> sut = new CsvSerializer<Person11>();

            Person11 p1 = new Person11 { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person11 p2 = new Person11 { Name = "Isabella", Gender = "Female", Age = 20 };

            Person11[] persons = { p1, p2 };

            using (FileStream stream = File.Create("data/output_2.1.csv"))
                sut.Serialize(stream, persons);

            using (StreamReader sr = File.OpenText("data/output_2.1.csv"))
            {
                string[] actual   = sr.ReadLine().Split(';');
                string[] expected = { "Name", "Sex", "Age" };

                CollectionAssert.AreEqual(expected, actual);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Lookup_and_DisplayName___Lookup_is_used()
        {
            Func<string, string> lookupDisplayName = prop =>
            {
                if (prop == "Gender") return "Male/Female";

                return prop;
            };

            Func<string, string> lookupPropertyName = display =>
            {
                if (display == "Male/Female") return "Gender";

                return display;
            };

            CsvSerializer<Person1> sut = new CsvSerializer<Person1>(
                lookupDisplayNameFromPropertyName: lookupDisplayName,
                lookupPropertyNameFromDisplayName: lookupPropertyName);

            Person1 p1 = new Person1 { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person1 p2 = new Person1 { Name = "Isabella", Gender = "Female", Age = 20 };

            Person1[] persons = { p1, p2 };         

            using (FileStream stream = File.Create("data/output_3.csv"))
                sut.Serialize(stream, persons);

            using (FileStream stream = File.OpenRead("data/output_3.csv"))
            {
                List<Person1> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                IEqualityComparer<Person1> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void SerializerIgnore___property_ignored()
        {
            CsvSerializer<Person2> sut = new CsvSerializer<Person2>();

            Person2 p1 = new Person2 { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person2 p2 = new Person2 { Name = "Isabella", Gender = "Female", Age = 20 };

            Person2[] persons = { p1, p2 };

            using (FileStream stream = File.Create("data/output_4.csv"))
                sut.Serialize(stream, persons);

            using (FileStream stream = File.OpenRead("data/output_4.csv"))
            {
                List<Person2> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                IEqualityComparer<Person2> personComparer = new PersonEqualityComparer();

                Assert.That(actual[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual[1], Is.EqualTo(p2).Using(personComparer));
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void DateTime_Issue4___Cell_shows_Date()
        {
            CsvSerializer<PersonDateTimeTest> sut = new CsvSerializer<PersonDateTimeTest>();

            PersonDateTimeTest p1 = new PersonDateTimeTest { Name = "Günther", DateOfBirth = new DateTime(1982, 7, 22, 23, 25, 30), Male = true, Weight = 105.2, Heigth = 184 };

            PersonDateTimeTest[] persons = { p1 };

            using (FileStream stream = File.Create("data/output_5.csv"))
                sut.Serialize(stream, persons);

            using (StreamReader sr = File.OpenText("data/output_5.csv"))
            {
                string line   = sr.ReadLine();        // Header
                line          = sr.ReadLine();
                string[] cols = line.Split(';');

                DateTime actual = DateTime.Parse(cols[2]);

                Assert.AreEqual(new DateTime(1982, 7, 22, 23, 25, 30), actual);
            }

            using (FileStream stream = File.OpenRead("data/output_5.csv"))
            {
                List<PersonDateTimeTest> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(1, actual.Count);
                Assert.AreEqual("Günther", actual[0].Name);
                Assert.AreEqual(new DateTime(1982, 7, 22, 23, 25, 30), actual[0].DateOfBirth);
                Assert.IsTrue(actual[0].Male);
                Assert.AreEqual(105.2, actual[0].Weight, 1e-3);
                Assert.AreEqual(184, actual[0].Heigth);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void DateTime_only_DatePart___OK()
        {
            CsvSerializer<PersonDateTimeTest1> sut = new CsvSerializer<PersonDateTimeTest1>();

            PersonDateTimeTest1 p1 = new PersonDateTimeTest1 { Name = "Günther", DateOfBirth = new DateTime(1982, 7, 22, 23, 25, 30), Male = true, Weight = 105.2, Heigth = 184 };

            PersonDateTimeTest1[] persons = { p1 };

            using (FileStream stream = File.Create("data/output_6.csv"))
                sut.Serialize(stream, persons);

            using (StreamReader sr = File.OpenText("data/output_6.csv"))
            {
                string line   = sr.ReadLine();        // Header
                line          = sr.ReadLine();
                string[] cols = line.Split(';');

                DateTime actual = DateTime.Parse(cols[2]);

                TestContext.WriteLine(System.Threading.Thread.CurrentThread.CurrentCulture);
                TestContext.WriteLine(System.Threading.Thread.CurrentThread.CurrentUICulture);
                TestContext.WriteLine($"cols[2] = {cols[2]}");

                if (string.IsNullOrWhiteSpace(Environment.GetEnvironmentVariable("FUCK_DATE")))
                    Assert.AreEqual(10, cols[2].Length);

                Assert.AreEqual(new DateTime(1982, 7, 22), actual);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void DateTime_with_Zero_Time___written_as_DateTime_with_Time()
        {
            CsvSerializer<PersonDateTimeTest> sut = new CsvSerializer<PersonDateTimeTest>();

            PersonDateTimeTest p1 = new PersonDateTimeTest { Name = "Günther", DateOfBirth = new DateTime(1982, 7, 22, 0, 0, 0), Male = true, Weight = 105.2, Heigth = 184 };

            PersonDateTimeTest[] persons = { p1 };

            using (FileStream stream = File.Create("data/output_7.csv"))
                sut.Serialize(stream, persons);

            using (StreamReader sr = File.OpenText("data/output_7.csv"))
            {
                string line   = sr.ReadLine();        // Header
                line          = sr.ReadLine();
                string[] cols = line.Split(';');

                DateTime actual = DateTime.Parse(cols[2]);

                if (string.IsNullOrWhiteSpace(Environment.GetEnvironmentVariable("FUCK_DATE")))
                    Assert.AreEqual(19, cols[2].Length);

                Assert.AreEqual(new DateTime(1982, 7, 22), actual);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Model_with_null_value_given___OK()
        {
            CsvSerializer<Person4> sut = new CsvSerializer<Person4>();

            Person4 p1 = new Person4 { Name = "Günther" , Gender = "Male"  , Age = null };
            Person4 p2 = new Person4 { Name = "Isabella", Gender = "Female", Age = 20 };

            Person4[] persons = { p1, p2 };

            using (FileStream stream = File.Create("data/output_8.csv"))
            {
                sut.Serialize(stream, persons);
            }

            using (FileStream stream = File.OpenRead("data/output_8.csv"))
            {
                List<Person4> actual = sut.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual.Count);

                Assert.IsNull(actual[0].Age);
                Assert.AreEqual(20, actual[1].Age);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Display_Order___order_is_in_output()
        {
            CsvSerializer<Person5> sut = new CsvSerializer<Person5>();

            Person5 p1 = new Person5 { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person5 p2 = new Person5 { Name = "Isabella", Gender = "Female", Age = 20 };

            Person5[] persons = { p1, p2 };

            using (FileStream stream = File.Create("data/output_9.csv"))
            {
                sut.Serialize(stream, persons);
            }

            using (StreamReader sr = File.OpenText("data/output_9.csv"))
            {
                string[] actual   = sr.ReadLine().Split(';');
                string[] expected = { "Name", "Age", "Gender" };

                CollectionAssert.AreEqual(expected, actual);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Serializer_for_two_different_types___OK()
        {
            CsvSerializer<Person> sut1             = new CsvSerializer<Person>();
            CsvSerializer<PersonDateTimeTest> sut2 = new CsvSerializer<PersonDateTimeTest>();

            Person p1 = new Person { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person p2 = new Person { Name = "Isabella", Gender = "Female", Age = 20 };

            PersonDateTimeTest pdt1 = new PersonDateTimeTest { Name = "Günther", DateOfBirth = new DateTime(1982, 7, 22, 0, 0, 0), Male = true, Weight = 105.2, Heigth = 184 };

            Person[] persons               = { p1, p2 };
            PersonDateTimeTest[] personsDt = { pdt1 };

            using (FileStream stream = File.Create("data/output_11.csv"))
                sut1.Serialize(stream, persons);

            using (FileStream stream = File.OpenRead("data/output_11.csv"))
            {
                List<Person> actual1 = sut1.Deserialize(stream).ToList();

                Assert.AreEqual(2, actual1.Count);

                IEqualityComparer<Person> personComparer = new PersonEqualityComparer();

                Assert.That(actual1[0], Is.EqualTo(p1).Using(personComparer));
                Assert.That(actual1[1], Is.EqualTo(p2).Using(personComparer));
            }

            using (FileStream stream = File.Create("data/output_12.csv"))
                sut2.Serialize(stream, personsDt);

            using (FileStream stream = File.OpenRead("data/output_12.csv"))
            {
                List<PersonDateTimeTest> actual2 = sut2.Deserialize(stream).ToList();

                Assert.AreEqual(1, actual2.Count);
            }
        }
        //---------------------------------------------------------------------
        [Test]
        public void Serialization_to_MemoryStream___Stream_is_not_disposed_Issue13()
        {
            CsvSerializer<Person> sut = new CsvSerializer<Person>();

            Person p1 = new Person { Name = "Günther" , Gender = "Male"  , Age = 32 };
            Person p2 = new Person { Name = "Isabella", Gender = "Female", Age = 20 };

            Person[] persons = { p1, p2 };

            using (MemoryStream stream = new MemoryStream())
            {
                sut.Serialize(stream, persons);

                Assert.That(stream.Length, Is.GreaterThan(0));
            }
        }
    }
}
