﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using gfoidl.Serializer.Csv;

namespace gfoidl.Serializer.Performance.TestDat
{
    static class Program
    {
        private const int     NoOfLines = 5_000_000;
        private const string  TestFile  = @"D:\Work-Git\gfoidl\gfoidl.Serializer\perf\data.txt";
        private static Random _random   = new Random();
        //---------------------------------------------------------------------
        static void Main(string[] args)
        {
            var serializer = new CsvSerializer<FakeModel>();

            using (FileStream fs = File.Create(TestFile))
                serializer.Serialize(fs, GetFakeModel());

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"\nTest-file written to {TestFile}");
            Console.ResetColor();
        }
        //---------------------------------------------------------------------
        private static IEnumerable<FakeModel> GetFakeModel()
        {
            for (int i = 0; i < NoOfLines; ++i)
            {
                yield return new FakeModel
                {
                    Id        = Guid.NewGuid(),
                    Name      = GetRandomString(),
                    No        = i + 1,
                    TimeStamp = DateTime.Now.AddHours(-_random.Next(0, 10_000)),
                    Value1    = _random.NextDouble(),
                    Value2    = _random.Next()
                };

                if (i % 10_000 == 0)
                    Console.Write('.');
            }

            Console.WriteLine();
        }
        //---------------------------------------------------------------------
        private static string GetRandomString(int minLength = 8, int maxLength = 30)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            var sb             = new StringBuilder();
            int length         = _random.Next(minLength, maxLength + 1);

            for (int i = 0; i < length; ++i)
            {
                int charIdx = _random.Next(chars.Length);
                char c      = chars[charIdx];
                sb.Append(c);
            }

            return sb.ToString();
        }
    }
}
