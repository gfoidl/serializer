﻿#define FAKE_MODEL
//#define COMPARE
#define MEMORY_STREAM
//-----------------------------------------------------------------------------
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using gfoidl.Serializer.Csv;
using TinyCsvParser;
using TinyCsvParser.Mapping;

namespace gfoidl.Serializer.Performance
{
    static class Program
    {
#if FAKE_MODEL
        private const string TestFile = @"D:\Work-Git\gfoidl\gfoidl.Serializer\perf\data.txt";
        private const char Separator = ';';
#else
        private const string TestFile = @"D:\Work-Git\gfoidl\gfoidl.Serializer\perf\201503remarks.txt";
        private const char Separator = ',';
#endif
        //---------------------------------------------------------------------
        static void Main(string[] args)
        {
#if MEMORY_STREAM
            MemoryStream stream = ReadToMemoryStream();
            GC.Collect(2, GCCollectionMode.Forced, true, true);
            GC.WaitForPendingFinalizers();
            GC.Collect();
#endif
#if FAKE_MODEL
            var serializer = new CsvSerializer<FakeModel>();
#else
            var serializer = new CsvSerializer<Model>();
#endif
            int count      = 0;

            Stopwatch sw = Stopwatch.StartNew();
#if !COMPARE
            for (int i = 0; i < 5; ++i)
            {
#endif
#if MEMORY_STREAM
                stream.Position = 0;
#else
                using (FileStream stream = File.OpenRead(TestFile))
#endif
                {
                    foreach (var m in serializer.Deserialize(stream, Separator, keepOrder: false))
                        count++;
                }
#if !COMPARE
            }
#endif
            sw.Stop();
#if COMPARE
            Console.WriteLine($"Time [ms]: {sw.ElapsedMilliseconds}");
#else
            Console.WriteLine($"Time [ms]: {sw.ElapsedMilliseconds * 0.2}");
#endif
#if COMPARE
            var csvParserOptions = new CsvParserOptions(true, Separator, Environment.ProcessorCount, false);
            var csvParserMapping = new CsvModelMapping();
#if FAKE_MODEL
            var csvParser = new CsvParser<FakeModel>(csvParserOptions, csvParserMapping);
#else
            var csvParser = new CsvParser<Model>(csvParserOptions, csvParserMapping);
#endif
            sw.Restart();
            {
                foreach (var item in csvParser.ReadFromFile(TestFile, Encoding.UTF8))
                {
                    var m = item.Result;
                    count++;
                }
            }
            sw.Stop();
            Console.WriteLine($"Time [ms]: {sw.ElapsedMilliseconds}");
#endif
        }
        //---------------------------------------------------------------------
#if MEMORY_STREAM
        private static MemoryStream ReadToMemoryStream()
        {
            using (FileStream fs = File.OpenRead(TestFile))
            {
                var ms = new MemoryStream();
                fs.CopyTo(ms);
                ms.Position = 0;

                return ms;
            }
        }
#endif
        //---------------------------------------------------------------------
#if FAKE_MODEL
        public class CsvModelMapping : CsvMapping<FakeModel>
        {
            public CsvModelMapping()
            {
                this.MapProperty(0, x => x.No);
                this.MapProperty(1, x => x.Id);
                this.MapProperty(2, x => x.Value1);
                this.MapProperty(3, x => x.Value2);
                this.MapProperty(4, x => x.Name);
                this.MapProperty(5, x => x.TimeStamp);
            }
        }
#else
        public class CsvModelMapping : CsvMapping<Model>
        {
            public CsvModelMapping()
            {
                this.MapProperty(0, x => x.WBAN);
                this.MapProperty(1, x => x.YearMonthDay);
                this.MapProperty(2, x => x.Time);
                this.MapProperty(3, x => x.Remarks);
            }
        }
#endif
    }
}
