﻿using System;

namespace gfoidl.Serializer.Performance
{
    public class Model
    {
        public string WBAN         { get; set; }
        public string YearMonthDay { get; set; }
        public string Time         { get; set; }
        public string Remarks      { get; set; }
    }
    //-------------------------------------------------------------------------
    public class FakeModel
    {
        public int No             { get; set; }
        public Guid Id            { get; set; }
        public double Value1      { get; set; }
        public int Value2         { get; set; }
        public string Name        { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
