﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using gfoidl.FastReflection;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace gfoidl.Serializer.Excel
{
    /// <summary>
    /// A serializer/deserializer for Excel (xslx).
    /// </summary>
    /// <typeparam name="T">The type of the model to be serialized/deserialized.</typeparam>
    public class ExcelSerializer<T> : Serializer<T> where T : class, new()
    {
        /// <summary>
        /// Creates a new instance of <see cref="ExcelSerializer{T}" />.
        /// </summary>
        /// <param name="lookupDisplayNameFromPropertyName">
        /// A <see cref="Func{String, String}" /> for looking up the display name (= column name)
        /// for a given property name.
        /// </param>
        /// <param name="lookupPropertyNameFromDisplayName">
        /// A <see cref="Func{String, String}" /> for looking up the property name for
        /// a given display name (= column name)
        /// </param>
        public ExcelSerializer(
            Func<string, string> lookupDisplayNameFromPropertyName = null,
            Func<string, string> lookupPropertyNameFromDisplayName = null)
            : base(lookupDisplayNameFromPropertyName, lookupPropertyNameFromDisplayName)
        { }
        //---------------------------------------------------------------------
        /// <summary>
        /// Serializes the <paramref name="model" /> to the given <paramref name="stream" />.
        /// </summary>
        /// <param name="stream">
        /// The <see cref="Stream" /> where the <paramref name="model" /> gets serialized.
        /// </param>
        /// <param name="model">The model to serialize.</param>
        /// <param name="sheetName">
        /// The name of the sheet in Excel. Defaults to <c>Data</c>.
        /// </param>
        /// <param name="autoSizeColumns">
        /// When <c>true</c> the columns are auto sized. Defaults to <c>true</c>.
        /// </param>
        /// <param name="setAutoFilter">
        /// When <c>true</c> sets the auto filter for the data. Defaults to <c>true</c>.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="stream" /> is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="model" /> is <c>null</c>.</exception>
        public void Serialize(Stream stream, IEnumerable<T> model, string sheetName = "Data", bool autoSizeColumns = true, bool setAutoFilter = true)
        {
            if (stream == null) ThrowHelper.ThrowArgumentNull(ExceptionArgument.stream);
            if (model  == null) ThrowHelper.ThrowArgumentNull(ExceptionArgument.model);

            this.SerializeCore(stream, model, sheetName, autoSizeColumns, setAutoFilter);
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Deserializes the <paramref name="stream" />.
        /// </summary>
        /// <param name="stream">
        /// The <see cref="Stream" /> from which the models are read / deserialized.
        /// </param>
        /// <param name="sheetIndex">
        /// The index of the sheet from where the data is read. Defaults to <c>0</c>.
        /// </param>
        /// <returns>A enumeration of read models from the <paramref name="stream" />.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="stream"/> is <c>null</c>.</exception>
        public IEnumerable<T> Deserialize(Stream stream, int sheetIndex = 0)
        {
            if (stream == null) ThrowHelper.ThrowArgumentNull(ExceptionArgument.stream);

            return this.DeserializeCore(stream, sheetIndex);
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Core implemntation of the serialization.
        /// </summary>
        /// <param name="stream">
        /// The <see cref="Stream" /> where the <paramref name="model" /> gets serialized.
        /// </param>
        /// <param name="model">The model to serialize.</param>
        protected override void SerializeCore(Stream stream, IEnumerable<T> model) => this.SerializeCore(stream, model);
        //---------------------------------------------------------------------
        private void SerializeCore(Stream stream, IEnumerable<T> model, string sheetName = "Data", bool autoSizeColumns = true, bool setAutoFilter = true)
        {
            IWorkbook workBook     = new XSSFWorkbook();
            ISheet sheet           = workBook.CreateSheet(sheetName) as XSSFSheet;
            string[] propertyNames = this.GetPropertyNames();

            this.WriteHeader(sheet, propertyNames);
            this.WriteData(model, sheet, propertyNames);

            if (autoSizeColumns && Environment.OSVersion.Platform == PlatformID.Win32NT)
                for (int j = 0; j < propertyNames.Length; ++j)
                    sheet.AutoSizeColumn(j);

            if (setAutoFilter)
                sheet.SetAutoFilter(new NPOI.SS.Util.CellRangeAddress(0, sheet.LastRowNum, 0, propertyNames.Length - 1));

            WriteToStream(stream, workBook);
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Core implementation of the deserialization.
        /// </summary>
        /// <param name="stream">
        /// The <see cref="Stream" /> from which the models are read / deserialized.
        /// </param>
        /// <returns>A enumeration of read models from the <paramref name="stream" /></returns>
        protected override IEnumerable<T> DeserializeCore(Stream stream) => this.DeserializeCore(stream);
        //---------------------------------------------------------------------
        private IEnumerable<T> DeserializeCore(Stream stream, int sheetIndex = 0)
        {
            IWorkbook workBook = WorkbookFactory.Create(stream);
            ISheet sheet       = workBook.GetSheetAt(sheetIndex);

            IRow headerRow  = sheet.GetRow(0);
            string[] header = headerRow.Cells
                .Select(c   => c.StringCellValue)
                .ToArray();

            (string Name, PropertyInfo Property, Delegate Setter)[] properties = this.MapHeaderToPropertyNames(header);

            for (int j = 1; j < sheet.LastRowNum + 1; ++j)
            {
                IRow row = sheet.GetRow(j);
                T item   = _fastCtor();

                for (int i = 0; i < properties.Length; ++i)
                {
                    ICell cell       = row.GetCell(i);
                    string cellValue = GetCellValueCore(cell, properties[i].Property.PropertyType);
                    this.SetPropertyValue(item, i, properties, cellValue);
                }

                yield return item;
            }
        }
        //---------------------------------------------------------------------
        private void WriteHeader(ISheet sheet, string[] propertyNames)
        {
            sheet.CreateFreezePane(0, 1);
            IRow headerRow = sheet.CreateRow(0);

            var style          = sheet.Workbook.CreateCellStyle();
            var font           = sheet.Workbook.CreateFont();
            font.IsBold        = true;
            style.SetFont(font);
            headerRow.RowStyle = style;

            for (int i = 0; i < propertyNames.Length; ++i)
            {
                ICell cell         = headerRow.CreateCell(i, CellType.String);
                string displayName = this.LookupDisplayName(propertyNames[i]);

                PropertyValue pv = _properties[propertyNames[i]];
                if (pv.HideColumn) sheet.SetColumnHidden(i, true);

                cell.SetCellValue(displayName);
            }
        }
        //---------------------------------------------------------------------
        private void WriteData(IEnumerable<T> model, ISheet sheet, string[] propertyNames)
        {
            int rowCounter = 0;

            foreach (T item in model)
            {
                IRow row = sheet.CreateRow(++rowCounter);

                for (int i = 0; i < propertyNames.Length; ++i)
                {
                    object value = this.GetPropertyValue(item, propertyNames[i]);
                    ICell cell   = row.CreateCell(i);
                    this.SetCellValue(cell, value, propertyNames[i]);
                }
            }
        }
        //---------------------------------------------------------------------
        private static void WriteToStream(Stream stream, IWorkbook workBook)
        {
            if (stream is MemoryStream)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    workBook.Write(ms);
                    byte[] buffer = ms.ToArray();
                    stream.Write(buffer, 0, buffer.Length);
                }
            }
            else
                workBook.Write(stream);
        }
        //---------------------------------------------------------------------
        private void SetCellValue(ICell cell, object value, string propertyName)
        {
            if (value == null) return;

            Type valueType = value.GetType();

            if (valueType == typeof(int) ||
                valueType == typeof(long) ||
                valueType == typeof(float) ||
                valueType == typeof(double))
            {
                TypeConverter converter = _converters[valueType];
                double val              = (double)converter.ConvertTo(value, typeof(double));
                cell.SetCellValue(val);
                return;
            }

            if (valueType == typeof(bool))
            {
                cell.SetCellValue((bool)value);
                return;
            }

            if (valueType == typeof(DateTime))
            {
                bool onlyDatePart;
                _dateTimeFormats.TryGetValue(propertyName, out onlyDatePart);

                DateTime dt      = (DateTime)value;
                string format    = onlyDatePart ? "DD/MM/YYYY" : @"DD/MM/YYYY\ HH:MM:SS";
                var dataFormat   = cell.Sheet.Workbook.CreateDataFormat();
                var style        = cell.Sheet.Workbook.CreateCellStyle();
                style.DataFormat = dataFormat.GetFormat(format);
                cell.CellStyle   = style;

                cell.SetCellValue(dt);
                return;
            }

            cell.SetCellValue(value.ToString());
        }
        //---------------------------------------------------------------------
        private static string GetCellValueCore(ICell cell, Type targetType, CellType? cellType = null)
        {
            if (cell == null) return null;

            switch (cellType ?? cell.CellType)
            {
                case CellType.Numeric:
                    if (targetType == typeof(DateTime))
                        return cell.DateCellValue.ToString();
                    else
                        return cell.NumericCellValue.ToString();
                case CellType.String : return cell.StringCellValue;
                case CellType.Blank  : return null;
                case CellType.Boolean: return cell.BooleanCellValue.ToString();
                case CellType.Formula:
                    return GetCellValueCore(cell, targetType, cell.CachedFormulaResultType);
                case CellType.Unknown:
                case CellType.Error  :
                    break;
            }

            ThrowInvalidOperationException();
            return null;
            
            void ThrowInvalidOperationException()
            {
                throw new InvalidOperationException(string.Format(Strings.CellType_cant_be_read, cell.CellType, cell.RowIndex, cell.ColumnIndex));
            }
        }
    }
}
