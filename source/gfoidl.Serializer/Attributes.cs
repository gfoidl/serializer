﻿using System;

namespace gfoidl.Serializer
{
    /// <summary>
    /// Added on a property indicates that the serializer should ignore this property.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public sealed class SerializerIgnoreAttribute : Attribute { }
    //-------------------------------------------------------------------------
    /// <summary>
    /// Specifies the format of <see cref="DateTime" /> for serialization.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public sealed class DateTimeFormatAttribute : Attribute
    {
        /// <summary>
        /// When <c>true</c> only the date part of the <see cref="DateTime" />
        /// will be emitted.
        /// </summary>
        public bool OnlyDatePart { get; }
        //---------------------------------------------------------------------
        /// <summary>
        /// Creates a new instance of <see cref="DateTimeFormatAttribute" />.
        /// </summary>
        /// <param name="onlyDatePart">
        /// When <c>true</c> only the date part of the <see cref="DateTime" />
        /// will be emitted.
        /// </param>
        public DateTimeFormatAttribute(bool onlyDatePart = false)
        {
            this.OnlyDatePart = onlyDatePart;
        }
    }
    //---------------------------------------------------------------------
    /// <summary>
    /// Added to a property indicates that the serializer should hide the property.
    /// </summary>
    /// <remarks>
    /// Has only effect on serialization to Excel.
    /// </remarks>
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public sealed class HideColumnAttribute : Attribute { }
}
