﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Reflection;
using gfoidl.FastReflection;

namespace gfoidl.Serializer
{
    /// <summary>
    /// A base class for serializers.
    /// </summary>
    /// <typeparam name="T">The type of the model to be serialized/deserialized.</typeparam>
    public abstract class Serializer<T> : ISerializer<T> where T : class, new()
    {
#pragma warning disable CS1591
        private static object                              _syncRoot              = new object();
        protected static readonly FastCtor<T>              _fastCtor              = FastReflectionHelper.GetFastCtor<T>();
        protected static Dictionary<string, PropertyValue> _properties            = new Dictionary<string, PropertyValue>();
        private static Dictionary<string, string>          _displayNameToProperty = new Dictionary<string, string>();
        private static Dictionary<string, string>          _propertyToDisplayName = new Dictionary<string, string>();
        protected static Dictionary<string, bool>          _dateTimeFormats       = new Dictionary<string, bool>();
        protected static Dictionary<Type, TypeConverter>   _converters            = new Dictionary<Type, TypeConverter>();
#pragma warning restore CS1591
        //---------------------------------------------------------------------
        /// <summary>
        /// A <see cref="Func{String, String}" /> for looking up the display name (= column name)
        /// for a given property name.
        /// </summary>
        public Func<string, string> LookupDisplayNameFromPropertyName { get; }
        //---------------------------------------------------------------------
        /// <summary>
        /// A <see cref="Func{String, String}" /> for looking up the property name for
        /// a given display name (= column name)
        /// </summary>
        public Func<string, string> LookupPropertyNameFromDisplayName { get; }
        //---------------------------------------------------------------------
        /// <summary>
        /// Creates an instance of <see cref="Serializer{T}" />.
        /// </summary>
        /// <param name="lookupDisplayNameFromPropertyName">
        /// A <see cref="Func{String, String}" /> for looking up the display name (= column name)
        /// for a given property name.
        /// </param>
        /// <param name="lookupPropertyNameFromDisplayName">
        /// A <see cref="Func{String, String}" /> for looking up the property name for
        /// a given display name (= column name)
        /// </param>
        protected Serializer(
            Func<string, string> lookupDisplayNameFromPropertyName = null,
            Func<string, string> lookupPropertyNameFromDisplayName = null)
        {
            this.LookupDisplayNameFromPropertyName = lookupDisplayNameFromPropertyName;
            this.LookupPropertyNameFromDisplayName = lookupPropertyNameFromDisplayName;

            if (_properties.Keys.Count > 0) return;

            lock (_syncRoot) this.EnsureInitialized();
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Serializes the <paramref name="model" /> to the given <paramref name="stream" />.
        /// </summary>
        /// <param name="stream">
        /// The <see cref="Stream" /> where the <paramref name="model" /> gets serialized.
        /// </param>
        /// <param name="model">The model to serialize.</param>
        /// <exception cref="ArgumentNullException"><paramref name="stream" /> is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="model" /> is <c>null</c>.</exception>
        public void Serialize(Stream stream, IEnumerable<T> model)
        {
            if (stream == null) ThrowHelper.ThrowArgumentNull(ExceptionArgument.stream);
            if (model  == null) ThrowHelper.ThrowArgumentNull(ExceptionArgument.model);

            this.SerializeCore(stream, model);
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Deserializes the <paramref name="stream" />.
        /// </summary>
        /// <param name="stream">
        /// The <see cref="Stream" /> from which the models are read / deserialized.
        /// </param>
        /// <returns>A enumeration of read models from the <paramref name="stream" />.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="stream"/> is <c>null</c>.</exception>
        public IEnumerable<T> Deserialize(Stream stream)
        {
            if (stream == null) ThrowHelper.ThrowArgumentNull(ExceptionArgument.stream);

            return this.DeserializeCore(stream);
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Core implemntation of the serialization.
        /// </summary>
        /// <param name="stream">
        /// The <see cref="Stream" /> where the <paramref name="model" /> gets serialized.
        /// </param>
        /// <param name="model">The model to serialize.</param>
        protected abstract void SerializeCore(Stream stream, IEnumerable<T> model);
        //---------------------------------------------------------------------
        /// <summary>
        /// Core implementation of the deserialization.
        /// </summary>
        /// <param name="stream">
        /// The <see cref="Stream" /> from which the models are read / deserialized.
        /// </param>
        /// <returns>A enumeration of read models from the <paramref name="stream" /></returns>
        protected abstract IEnumerable<T> DeserializeCore(Stream stream);
        //---------------------------------------------------------------------
        private void EnsureInitialized()
        {
            // Double check
            if (_properties.Keys.Count > 0) return;

            var type       = typeof(T);
            var properties = type.GetProperties(
                BindingFlags.Public |
                BindingFlags.Instance |
                BindingFlags.GetProperty |
                BindingFlags.SetProperty);

            foreach (var p in properties)
            {
                if (p.GetCustomAttribute<SerializerIgnoreAttribute>() != null) continue;

                var pv              = new PropertyValue(p);
                _properties[p.Name] = pv;

                DisplayNameAttribute displayName = p.GetCustomAttribute<DisplayNameAttribute>();
                if (displayName != null)
                    this.StoreDisplayName(displayName.DisplayName, p.Name);

                DisplayAttribute display = p.GetCustomAttribute<DisplayAttribute>();
                if (display != null)
                {
                    if (!string.IsNullOrWhiteSpace(display.Name))
                        this.StoreDisplayName(display.Name, p.Name);

                    pv.Order = display.GetOrder();
                }

                HideColumnAttribute hideColumn = p.GetCustomAttribute<HideColumnAttribute>();
                if (hideColumn != null)
                    pv.HideColumn = true;

                Type propertyType = p.PropertyType;

                if (propertyType == typeof(DateTime))
                {
                    DateTimeFormatAttribute dateTimeFormat = p.GetCustomAttribute<DateTimeFormatAttribute>();
                    if (dateTimeFormat != null)
                        _dateTimeFormats[p.Name] = dateTimeFormat.OnlyDatePart;
                }

                if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    propertyType = propertyType.GenericTypeArguments[0];

                if (!_converters.ContainsKey(propertyType))
                    _converters[propertyType] = TypeDescriptor.GetConverter(propertyType);
            }
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Gets the property names of <typeparamref name="T" />.
        /// </summary>
        /// <returns>The property names of <typeparamref name="T" />.</returns>
        protected string[] GetPropertyNames()
        {
            return _properties
                .OrderBy(k => k.Value.Order)
                .Select(k => k.Key)
                .ToArray();
        }
        //---------------------------------------------------------------------
        private void StoreDisplayName(string displayName, string propertyName)
        {
            _displayNameToProperty[displayName]  = propertyName;
            _propertyToDisplayName[propertyName] = displayName;
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Sets the property value on the given <paramref name="item" />.
        /// </summary>
        /// <param name="item">The item / model on which the property is set.</param>
        /// <param name="propertyName">The name of the property to set.</param>
        /// <param name="value">The value to set.</param>
        protected void SetPropertyValue(T item, string propertyName, object value)
        {
            PropertyInfo pi = _properties[propertyName].PropertyInfo;
            value           = GetCellValueInTargetType(value, pi.PropertyType);

            if (value == null) return;

            this.SetPropertyValueCore(item, propertyName, value);
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Sets the property value on the given <paramref name="item" />.
        /// </summary>
        /// <param name="item">The item / model on which the property is set.</param>
        /// <param name="colIdx">The column-index corresponding to the property.</param>
        /// <param name="properties">"property-information"</param>
        /// <param name="value">The value to set.</param>
        protected void SetPropertyValue(
            T item,
            int colIdx,
            (string Name, PropertyInfo Property, Delegate Setter)[] properties,
            string value)
        {
            Type propertyType = properties[colIdx].Property.PropertyType;
            Delegate setter   = properties[colIdx].Setter;

            if (propertyType == typeof(string))
            {
                (setter as FastSetter)(item, value);
                return;
            }
            else if (propertyType == typeof(int) && int.TryParse(value, out int i))
            {
                (setter as FastSetter<int>)(item, i);
                return;
            }
            else if (propertyType == typeof(double) && double.TryParse(value, out double d))
            {
                (setter as FastSetter<double>)(item, d);
                return;
            }
            else if (propertyType==typeof(decimal) && decimal.TryParse(value, out decimal de))
            {
                (setter as FastSetter<decimal>)(item, de);
                return;
            }
            else if (propertyType == typeof(DateTime) && DateTime.TryParse(value, out DateTime dt))
            {
                (setter as FastSetter<DateTime>)(item, dt);
                return;
            }
            else if (propertyType == typeof(Guid) && Guid.TryParse(value, out Guid g))
            {
                (setter as FastSetter<Guid>)(item, g);
                return;
            }
            else if (propertyType == typeof(bool))
            {
                bool b = default;
                if (value == "1" || value == "true" || value == "True" || value == "ok")
                    b = true;

                (setter as FastSetter<bool>)(item, b);
                return;
            }

            this.SetPropertyValue(item, properties[colIdx].Name, value);
        }
        //---------------------------------------------------------------------
        private void SetPropertyValueCore(T item, string propertyName, object value)
        {
            FastSetter setter = FastReflectionHelper.GetFastSetter(typeof(T), propertyName);
            setter(item, value);
        }
        //---------------------------------------------------------------------
        private static object GetCellValueInTargetType(object value, Type targetType)
        {
            if (value == null) return null;

            Type sourceType = value.GetType();

            if (sourceType == typeof(string) && string.IsNullOrEmpty(value as string))
                return null;

            if (sourceType == targetType) return value;

            return Slow();
            //-----------------------------------------------------------------
            object Slow()
            {
                if (targetType.IsGenericType && targetType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    targetType = targetType.GenericTypeArguments[0];

                TypeConverter converter = _converters[targetType];

                if (converter.CanConvertFrom(sourceType))
                    return converter.ConvertFrom(value);

                if (converter.CanConvertTo(targetType))
                    return converter.ConvertTo(value, targetType);

                throw new InvalidOperationException($"Can't convert from {value} | {sourceType} to {targetType}");
            }
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Gets the property value from <paramref name="item" />.
        /// </summary>
        /// <param name="item">The item / model from which the property is read.</param>
        /// <param name="propertyName">The name of the property to read.</param>
        /// <returns>The property value.</returns>
        protected object GetPropertyValue(T item, string propertyName)
        {
            FastGetter getter = FastReflectionHelper.GetFastGetter(typeof(T), propertyName);
            object value      = getter(item);

            if (value is DateTime)
            {
                _dateTimeFormats.TryGetValue(propertyName, out bool onlyDatePart);

                if (onlyDatePart) return ((DateTime)value).Date;
            }

            return value;
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Creates property-infos according to the order in the header-column.
        /// </summary>
        /// <param name="header">The header column.</param>
        /// <returns>property-infos in order of header-columns</returns>
        protected (string Name, PropertyInfo Property, Delegate Setter)[] MapHeaderToPropertyNames(string[] header)
        {
            var properties = new(string, PropertyInfo, Delegate)[header.Length];

            for (int i = 0; i < header.Length; ++i)
            {
                string name     = this.LookupPropertyName(header[i]);
                PropertyInfo pi = _properties[name].PropertyInfo;
                Delegate fs     = FastReflectionHelper.GetFastSetter<T>(name);

                properties[i] = (name, pi, fs);
            }

            return properties;
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Looks up the display name from the <paramref name="propertyName" />.
        /// </summary>
        /// <param name="propertyName">The property name.</param>
        /// <returns>The display name, that will be used as column name.</returns>
        protected string LookupDisplayName(string propertyName)
        {
            string displayName;

            if (this.LookupDisplayNameFromPropertyName != null)
                return this.LookupDisplayNameFromPropertyName(propertyName);

            if (!_propertyToDisplayName.TryGetValue(propertyName, out displayName))
                displayName = propertyName;

            return displayName;
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Looks up the property name from the <paramref name="displayName" />.
        /// </summary>
        /// <param name="displayName">The display name (= column name).</param>
        /// <returns>The property name in <typeparamref name="T" />.</returns>
        /// <exception cref="PropertyForColumnNotFoundException{T}">
        /// <typeparamref name="T" /> has no property with the looked up name.
        /// </exception>
        protected string LookupPropertyName(string displayName)
        {
            string propertyName = this.LookupPropertyNameCore(displayName);

            if (!_properties.ContainsKey(propertyName)) ThrowHelper.ThrowPropertyForColumnNotFound<T>(propertyName);

            return propertyName;
        }
        //---------------------------------------------------------------------
        private string LookupPropertyNameCore(string displayName)
        {
            string propertyName;

            if (this.LookupPropertyNameFromDisplayName != null)
                return this.LookupPropertyNameFromDisplayName(displayName);

            if (!_displayNameToProperty.TryGetValue(displayName, out propertyName))
                propertyName = displayName;

            return propertyName;
        }
    }
}
