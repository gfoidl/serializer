﻿using System;
using System.Runtime.CompilerServices;

namespace gfoidl.Serializer
{
    internal static class ThrowHelper
    {
        public static void ThrowArgumentNull(ExceptionArgument exceptionArgument) => throw CreateArgumentNull(exceptionArgument);

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static Exception CreateArgumentNull(ExceptionArgument exceptionArgument) => new ArgumentNullException(exceptionArgument.ToString());
        //---------------------------------------------------------------------
        public static void ThrowPropertyForColumnNotFound<T>(string propertyName)
            where T : class
            => throw CreatePropertyForColumnNotFound<T>(propertyName);

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static Exception CreatePropertyForColumnNotFound<T>(string propertyName)
            where T : class
            => new PropertyForColumnNotFoundException<T>(propertyName);
    }
    //-------------------------------------------------------------------------
    internal enum ExceptionArgument
    {
        model,
        stream
    }
}
