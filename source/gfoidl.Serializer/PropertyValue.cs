﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace gfoidl.Serializer
{
#pragma warning disable CS1591
    [DebuggerDisplay("{PropertyInfo} | {Order}")]
    public class PropertyValue
    {
        public PropertyInfo PropertyInfo { get; set; }
        public int? Order                { get; set; }
        public bool HideColumn           { get; set; }
        //---------------------------------------------------------------------
        public PropertyValue(PropertyInfo propertyInfo, int? order = null, bool hideColumn = false)
        {
            this.PropertyInfo = propertyInfo ?? throw new ArgumentNullException(nameof(propertyInfo));
            this.Order        = order;
            this.HideColumn   = hideColumn;
        }
    }
#pragma warning restore CS1591
}
