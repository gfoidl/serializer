﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("gfoidl.Serializer.Csv")]
[assembly: InternalsVisibleTo("gfoidl.Serializer.Excel")]
