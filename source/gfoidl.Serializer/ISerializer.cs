﻿using System;
using System.Collections.Generic;
using System.IO;

namespace gfoidl.Serializer
{
    /// <summary>
    /// A basic interface for serializers.
    /// </summary>
    /// <typeparam name="T">The type of the model to be serialized/deserialized.</typeparam>
    public interface ISerializer<T> where T : class, new()
    {
        /// <summary>
        /// A <see cref="Func{String, String}" /> for looking up the display name (= column name)
        /// for a given property name.
        /// </summary>
        Func<string, string> LookupDisplayNameFromPropertyName { get; }
        //---------------------------------------------------------------------
        /// <summary>
        /// A <see cref="Func{String, String}" /> for looking up the property name for
        /// a given display name (= column name)
        /// </summary>
        Func<string, string> LookupPropertyNameFromDisplayName { get; }
        //---------------------------------------------------------------------
        /// <summary>
        /// Serializes the <paramref name="model" /> to the given <paramref name="stream" />.
        /// </summary>
        /// <param name="stream">
        /// The <see cref="Stream" /> where the <paramref name="model" /> gets serialized.
        /// </param>
        /// <param name="model">The model to serialize.</param>
        void Serialize(Stream stream, IEnumerable<T> model);
        //---------------------------------------------------------------------
        /// <summary>
        /// Deserializes the <paramref name="stream" />.
        /// </summary>
        /// <param name="stream">
        /// The <see cref="Stream" /> from which the models are read / deserialized.
        /// </param>
        /// <returns>A enumeration of read models from the <paramref name="stream" />.</returns>
        IEnumerable<T> Deserialize(Stream stream);
    }
}
