﻿using System;

namespace gfoidl.Serializer
{
    /// <summary>
    /// An <see cref="Exception" /> indication that the property is not found on
    /// <typeparamref name="T" />.
    /// </summary>
    /// <typeparam name="T">The model on which the property should be, but not found.</typeparam>
    [Serializable]
    public class PropertyForColumnNotFoundException<T> : Exception where T : class
    {
#pragma warning disable CS1591
        public PropertyForColumnNotFoundException() { }
        public PropertyForColumnNotFoundException(string message, Exception inner) : base(message, inner) { }
        protected PropertyForColumnNotFoundException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
        public PropertyForColumnNotFoundException(string columnName)
            : base(
                string.Format(Strings.Column_no_Property_in_Type, columnName, typeof(T)))
        { }
#pragma warning restore CS1591
    }
}
