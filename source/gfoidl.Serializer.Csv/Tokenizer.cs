﻿using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace gfoidl.Serializer.Csv
{
    internal class Tokenizer
    {
        private readonly TextReader                     _tr;
        private readonly int                            _cols;
        private readonly char                           _separator;
        private readonly int                            _charBufferSize;
        private char[]                                  _charBuffer;
        private readonly StringBuilder                  _sb;
        private readonly BlockingCollection<BufferItem> _buffer;
        private readonly IEnumerator<BufferItem>        _bufferEnumerator;
        private int                                     _posInBuffer;
        private int                                     _charsInBuffer;
        //---------------------------------------------------------------------
        public Tokenizer(TextReader tr, int cols, char separator, int charBufferSize = 1024, int producerCapacity = 750)
        {
            _tr               = tr;
            _cols             = cols;
            _separator        = separator;
            _charBufferSize   = charBufferSize;
            _sb               = new StringBuilder(_charBufferSize);
            _buffer           = new BlockingCollection<BufferItem>(producerCapacity);
            _bufferEnumerator = _buffer.GetConsumingEnumerable().GetEnumerator();

            var producer = Task.Factory.StartNew(this.ProduceCharBuffers);      // no longrunning here
        }
        //---------------------------------------------------------------------
        private void ProduceCharBuffers()
        {
            while (true)
            {
                char[] buffer = ArrayPool<char>.Shared.Rent(_charBufferSize);
                int read      = _tr.Read(buffer, 0, _charBufferSize);

                if (read == 0) break;

                var bufferItem = new BufferItem(read, buffer);
                _buffer.Add(bufferItem);
            }

            _buffer.CompleteAdding();
        }
        //---------------------------------------------------------------------
        public IEnumerable<Line> ParseLines()
        {
            this.ReadBuffer();

            if (_charsInBuffer == 0) yield break;

            do
            {
                Line line = this.ReadLine();

                if (line.IsEmptyLine) yield break;
                yield return line;

                if (_posInBuffer < _charsInBuffer) continue;
                if (_charsInBuffer == 0) yield break;

                this.ReadBuffer();      // don't merge to while-condition, 'cause of continue
            } while (_charsInBuffer > 0);
        }
        //-----------------------------------------------------------------
        private Line ReadLine()
        {
            var tokens        = new Token[_cols];
            int charPosInLine = 0;
            int colStart      = 0;
            int curCol        = 0;
            _sb.Clear();

            do
            {
                var charBuffer = new ReadOnlySpan<char>(_charBuffer, _posInBuffer, _charsInBuffer - _posInBuffer);

                int i = 0;
                for (; i < charBuffer.Length - 8; ++i)
                {
                    if (this.ReadLineLoopBody(charBuffer[i], i, ref curCol, ref colStart, ref charPosInLine, tokens)) goto NewLine;

                    i++;
                    if (this.ReadLineLoopBody(charBuffer[i], i, ref curCol, ref colStart, ref charPosInLine, tokens)) goto NewLine;

                    i++;
                    if (this.ReadLineLoopBody(charBuffer[i], i, ref curCol, ref colStart, ref charPosInLine, tokens)) goto NewLine;

                    i++;
                    if (this.ReadLineLoopBody(charBuffer[i], i, ref curCol, ref colStart, ref charPosInLine, tokens)) goto NewLine;

                    i++;
                    if (this.ReadLineLoopBody(charBuffer[i], i, ref curCol, ref colStart, ref charPosInLine, tokens)) goto NewLine;

                    i++;
                    if (this.ReadLineLoopBody(charBuffer[i], i, ref curCol, ref colStart, ref charPosInLine, tokens)) goto NewLine;

                    i++;
                    if (this.ReadLineLoopBody(charBuffer[i], i, ref curCol, ref colStart, ref charPosInLine, tokens)) goto NewLine;

                    i++;
                    if (this.ReadLineLoopBody(charBuffer[i], i, ref curCol, ref colStart, ref charPosInLine, tokens)) goto NewLine;
                }

                for (; i < charBuffer.Length - 4; ++i)
                {
                    if (this.ReadLineLoopBody(charBuffer[i], i, ref curCol, ref colStart, ref charPosInLine, tokens)) goto NewLine;

                    i++;
                    if (this.ReadLineLoopBody(charBuffer[i], i, ref curCol, ref colStart, ref charPosInLine, tokens)) goto NewLine;

                    i++;
                    if (this.ReadLineLoopBody(charBuffer[i], i, ref curCol, ref colStart, ref charPosInLine, tokens)) goto NewLine;

                    i++;
                    if (this.ReadLineLoopBody(charBuffer[i], i, ref curCol, ref colStart, ref charPosInLine, tokens)) goto NewLine;
                }

                for (; i < charBuffer.Length; ++i)
                {
                    if (this.ReadLineLoopBody(charBuffer[i], i, ref curCol, ref colStart, ref charPosInLine, tokens)) goto NewLine;
                }

                _sb.Append(_charBuffer, _posInBuffer, i);

            } while (this.ReadBuffer() > 0);

            tokens[curCol] = new Token(colStart, charPosInLine - colStart);
            goto NewLine;

        NewLine:        // Workaround for https://github.com/dotnet/coreclr/issues/13549
            return new Line(_sb, tokens);
        }
        //---------------------------------------------------------------------
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool ReadLineLoopBody(
            in char c,
            in int i,
            ref int curCol,
            ref int colStart,
            ref int charPosInLine,
            Token[] tokens)
        {
            if (c == '\r' || c == '\n')
            {
                tokens[curCol] = new Token(colStart, charPosInLine - colStart);
                _sb.Append(_charBuffer, _posInBuffer, i);

                _posInBuffer += i + 1;

                if (c == '\r' && (_posInBuffer < _charsInBuffer || this.ReadBuffer() > 0))
                    if (_charBuffer[_posInBuffer] == '\n') _posInBuffer++;

                return true;
            }
            else if (c == _separator)
            {
                tokens[curCol++] = new Token(colStart, charPosInLine - colStart);
                colStart         = charPosInLine + 1;
            }

            charPosInLine++;
            return false;
        }
        //---------------------------------------------------------------------
        private int ReadBuffer()
        {
            if (_charBuffer != null) ArrayPool<char>.Shared.Return(_charBuffer);

            if (_bufferEnumerator.MoveNext())
            {
                BufferItem bufferItem = _bufferEnumerator.Current;
                _charBuffer           = bufferItem.Chars;
                _charsInBuffer        = bufferItem.CharsInBuffer;
                _posInBuffer          = 0;

                return _charsInBuffer;
            }

            _charsInBuffer = 0;
            return _charsInBuffer;
        }
    }
}
