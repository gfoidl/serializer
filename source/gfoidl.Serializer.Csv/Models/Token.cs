﻿using System.Diagnostics;

namespace gfoidl.Serializer.Csv
{
    [DebuggerNonUserCode]
    [DebuggerDisplay("Start: {Start} | Length: {Length}")]
    internal readonly struct Token
    {
        public int Start  { get; }
        public int Length { get; }
        //---------------------------------------------------------------------
        public Token(int start, int length)
        {
            this.Start  = start;
            this.Length = length;
        }
    }
}
