﻿using System.Diagnostics;

namespace gfoidl.Serializer.Csv
{
    [DebuggerNonUserCode]
    [DebuggerDisplay("ColIdx: {ColIdx} | Value: {Value}")]
    internal readonly struct Column
    {
        public int ColIdx   { get; }
        public string Value { get; }
        //---------------------------------------------------------------------
        private Column(int colIdx)
        {
            this.ColIdx = colIdx;
            this.Value  = null;
        }
        //---------------------------------------------------------------------
        public Column(int colIdx, string value)
            : this(colIdx)
            => this.Value = value;
        //---------------------------------------------------------------------
        public Column(int colIdx, char[] chars, in Token token)
            : this(colIdx)
            => this.Value = new string(chars, token.Start, token.Length);
    }
}
