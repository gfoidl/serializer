﻿using System.Text;

namespace gfoidl.Serializer.Csv
{
    internal readonly struct Line
    {
        public char[] Content { get; }
        public Token[] Tokens { get; }
        //---------------------------------------------------------------------
        public bool IsEmptyLine => this.Content.Length == 0;
        //---------------------------------------------------------------------
        public Line(char[] content, Token[] tokens)
        {
            this.Content = content;
            this.Tokens  = tokens;
        }
        //---------------------------------------------------------------------
        public Line(StringBuilder sb, Token[] tokens)
        {
            var tmp = new char[sb.Length];
            sb.CopyTo(0, tmp, 0, tmp.Length);

            this.Content = tmp;
            this.Tokens  = tokens;
        }
        //---------------------------------------------------------------------
        public int ColumnCount         => this.Tokens.Length;
        public Column this[int colIdx] => new Column(colIdx, this.Content, this.Tokens[colIdx]);
        //---------------------------------------------------------------------
        // For Testing:
        internal Column[] GetColumns()
        {
            var columns = new Column[this.Tokens.Length];

            for (int i = 0; i < this.Tokens.Length; ++i)
                columns[i] = this[i];

            return columns;
        }
    }
}
