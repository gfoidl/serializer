﻿using System.Diagnostics;

namespace gfoidl.Serializer.Csv
{
    [DebuggerNonUserCode]
    [DebuggerDisplay("CharsInBuffer = {CharsInBuffer}")]
    internal readonly struct BufferItem
    {
        public int CharsInBuffer { get; }
        public char[] Chars      { get; }
        //---------------------------------------------------------------------
        public BufferItem(int charsInBuffer, char[] chars)
        {
            this.CharsInBuffer = charsInBuffer;
            this.Chars         = chars;
        }
    }
}
