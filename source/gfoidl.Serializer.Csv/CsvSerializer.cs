﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace gfoidl.Serializer.Csv
{
    /// <summary>
    /// A serializer for CSV-files.
    /// </summary>
    /// <typeparam name="T">The type of the model to be serialized/deserialized.</typeparam>
    public class CsvSerializer<T> : Serializer<T> where T : class, new()
    {
        /// <summary>
        /// Creates a new instance of <see cref="CsvSerializer{T}" />.
        /// </summary>
        /// <param name="lookupDisplayNameFromPropertyName">
        /// A <see cref="Func{String, String}" /> for looking up the display name (= column name)
        /// for a given property name.
        /// </param>
        /// <param name="lookupPropertyNameFromDisplayName">
        /// A <see cref="Func{String, String}" /> for looking up the property name for
        /// a given display name (= column name)
        /// </param>
        public CsvSerializer(
            Func<string, string> lookupDisplayNameFromPropertyName = null,
            Func<string, string> lookupPropertyNameFromDisplayName = null)
            : base(lookupDisplayNameFromPropertyName, lookupPropertyNameFromDisplayName)
        { }
        //---------------------------------------------------------------------
        /// <summary>
        /// Serializes the <paramref name="model" /> to the given <paramref name="stream" />.
        /// </summary>
        /// <param name="stream">
        /// The <see cref="Stream" /> where the <paramref name="model" /> gets serialized.
        /// </param>
        /// <param name="model">The model to serialize.</param>
        /// <param name="separator">The separtor in the csv-file. Defaults to <c>;</c></param>
        /// <param name="encoding">The <see cref="Encoding" />. Defaults to <see cref="Encoding.UTF8" />.</param>
        /// <exception cref="ArgumentNullException"><paramref name="stream" /> is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="model" /> is <c>null</c>.</exception>
        public void Serialize(Stream stream, IEnumerable<T> model, char separator = ';', Encoding encoding = null)
        {
            if (stream == null) ThrowHelper.ThrowArgumentNull(ExceptionArgument.stream);
            if (model  == null) ThrowHelper.ThrowArgumentNull(ExceptionArgument.model);

            this.SerializeCore(stream, model, separator, encoding);
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Deserializes the <paramref name="stream" />.
        /// </summary>
        /// <param name="stream">
        /// The <see cref="Stream" /> from which the models are read / deserialized.
        /// </param>
        /// <param name="separator"></param>
        /// <param name="keepOrder"></param>
        /// <param name="encoding"></param>
        /// <returns>A enumeration of read models from the <paramref name="stream" />.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="stream"/> is <c>null</c>.</exception>
        public IEnumerable<T> Deserialize(Stream stream, char separator = ';', bool keepOrder = true, Encoding encoding = null)
        {
            if (stream == null) ThrowHelper.ThrowArgumentNull(ExceptionArgument.stream);

            return this.DeserializeCore(stream, separator, keepOrder, encoding ?? Encoding.UTF8);
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Core implemntation of the serialization.
        /// </summary>
        /// <param name="stream">
        /// The <see cref="Stream" /> where the <paramref name="model" /> gets serialized.
        /// </param>
        /// <param name="model">The model to serialize.</param>
        protected override void SerializeCore(Stream stream, IEnumerable<T> model) => this.SerializeCore(stream, model);
        //---------------------------------------------------------------------
        private void SerializeCore(Stream stream, IEnumerable<T> model, char separator = ';', Encoding encoding = null)
        {
            StreamWriter sw = new StreamWriter(stream, encoding ?? Encoding.UTF8);

            string[] propertyNames = this.GetPropertyNames();

            this.WriteHeader(sw, propertyNames, separator);
            this.WriteData(model, sw, propertyNames, separator);

            sw.Flush();
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Core implementation of the deserialization.
        /// </summary>
        /// <param name="stream">
        /// The <see cref="Stream" /> from which the models are read / deserialized.
        /// </param>
        /// <returns>A enumeration of read models from the <paramref name="stream" /></returns>
        protected override IEnumerable<T> DeserializeCore(Stream stream) => this.DeserializeCore(stream);
        //---------------------------------------------------------------------
        private IEnumerable<T> DeserializeCore(
            Stream   stream,
            char     separator = ';',
            bool     keepOrder = true,
            Encoding encoding  = null)
        {
            StreamReader sr     = new StreamReader(stream, encoding ?? Encoding.UTF8);
            string[] header     = sr.ReadLine().Split(separator);
            Tokenizer tokenizer = new Tokenizer(sr, header.Length, separator);
            // After Ctor of Tokenizer, because the Tokenizer can start producing
            (string Name, PropertyInfo Property, Delegate Setter)[] properties = this.MapHeaderToPropertyNames(header);

            ParallelQuery<Line> lines = tokenizer
                .ParseLines()
                .AsParallel();

            if (keepOrder)
            {
                return lines
                    .Select((l, i) => (Idx: i, Content: l))
                    .OrderBy(r => r.Idx)
                    .Select(r => ProcessLine(r.Content));
            }

            return lines.Select(l => ProcessLine(l));
            //-----------------------------------------------------------------
            T ProcessLine(in Line line)
            {
                T item       = _fastCtor();
                int colCount = line.ColumnCount;

                for (int i = 0; i < colCount; ++i)
                {
                    Column column = line[i];
                    this.SetPropertyValue(item, column.ColIdx, properties, column.Value);
                }

                return item;
            }
        }
        //---------------------------------------------------------------------
        private void WriteHeader(StreamWriter sw, string[] propertyNames, char separator)
        {
            for (int i = 0; i < propertyNames.Length; ++i)
            {
                string displayName = this.LookupDisplayName(propertyNames[i]);
                sw.Write(displayName);

                if (i < propertyNames.Length - 1) sw.Write(separator);
            }

            sw.WriteLine();
        }
        //---------------------------------------------------------------------
        private void WriteData(IEnumerable<T> model, StreamWriter sw, string[] propertyNames, char separator)
        {
            foreach (T item in model)
            {
                for (int i = 0; i < propertyNames.Length; ++i)
                {
                    object value = this.GetPropertyValue(item, propertyNames[i]);

                    if (value is DateTime dt)
                        this.WriteDateTime(sw, dt, propertyNames[i]);
                    else
                        sw.Write(value);

                    if (i < propertyNames.Length - 1) sw.Write(separator);
                }

                sw.WriteLine();
            }
        }
        //---------------------------------------------------------------------
        private void WriteDateTime(StreamWriter sw, DateTime dateTime, string propertyName)
        {
            _dateTimeFormats.TryGetValue(propertyName, out bool onlyDatePart);

            if (onlyDatePart)
                sw.Write(dateTime.ToShortDateString());
            else
                sw.Write(dateTime.ToString());
        }
    }
}
